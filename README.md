# KFC SQL Database 

- This repository is a display of a SQL project based on the KFC business. By analysing the business rules and processes of KFC, a conceptual data model has been created in 'KFC conceptual data modelling.pdf'. This is an attempt to model an ERD based on the KFC business and its data needs. 
The 'KFC SQL questions & answers.pdf' file attempts to show an example of SQL commands that will be done in certain scenarios. 

- The objective of this project is to display skills in data modelling and SQL commands as well as understanding the processes of a businesss. 

